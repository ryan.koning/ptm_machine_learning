import os
import csv

path = './output'
csv_name = 'ppir_interactions.csv'

dirlist = [ item for item in os.listdir(path) if os.path.isdir(os.path.join(path, item)) ]


pdbs = []

# for dir in dirlist:
	# for filename in os.listdir(path + '/' + dir):
		# if filename.partition('.')[-1] == 'pdb':
			# pdbs.append(filename)

# print(pdbs)

atoms = []
for dir in dirlist:
	for filename in os.listdir(path + '/' + dir):
		if filename.partition('.')[-1] == 'pdb' and 'constrained' in filename:
			with open(path + '/' + dir + '/' + filename) as file:
				lines = file.readlines()
			for line in lines:
				 if 'ATOM' in line[0:10]:
					  atom = {'serial': int(line[6:11]),
								 'name': line[12:15],
								 'alternate_location': line[16], 
								 'residue_name': line[17:20],
								 'chain_id': line[21],
								 'residue_sequence': int(line[22:26]),
								 'residue_insertion_code': line[27],
								 'x': float(line[30:38]),
								 'y': float(line[38:46]),
								 'z': float(line[46:54]),
								 'occupancy': float(line[54:60]),
								 'temperature_factor': float(line[60:66]),
								 'element': line[76:78],
								 'charge': line[78:80].rstrip()}
					  atoms.append(atom)	

with open(csv_name, 'w') as write_to:
	fieldnames = ['serial',	'name', 'alternate_location','residue_name', 'chain_id', 'residue_sequence', 'residue_insertion_code', 'x','y','z', 'occupancy', 'temperature_factor', 'element', 'charge']
	wr = csv.DictWriter(write_to, fieldnames=fieldnames,lineterminator = '\n')
	wr.writeheader()
	for atom in atoms:
		wr.writerow(atom)	