import csv

#path = '/Users/Katherine/Downloads/pdb_utils/2BQ0.pdb'
path = './PDBs/1pa6.pdb'
with open(path) as file:
    lines = file.readlines()

atoms = []

for line in lines:
    if 'ATOM' in line[0:10]:
        atom = {'serial': int(line[6:11]),
                'name': line[12:15],
                'alternate_location': line[16], 
                'residue_name': line[17:20],
                'chain_id': line[21],
                'residue_sequence': int(line[22:26]),
                'residue_insertion_code': line[27],
                'x': float(line[30:38]),
                'y': float(line[38:46]),
                'z': float(line[46:54]),
                'occupancy': float(line[54:60]),
                'temperature_factor': float(line[60:66]),
                'element': line[76:78],
                'charge': line[78:80]}
        atoms.append(atom)

# for atom in atoms:
    # for attr in atom:
        # print('{:<22s} {:>12s}'.format(attr,str(atom[attr])))
    # print('\n')
