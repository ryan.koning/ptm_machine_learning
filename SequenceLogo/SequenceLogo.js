
var SequenceLogo = function (isClassic) {
	const height = 400
	this.isClassic = isClassic;
	this.sequences = [];
	
	function probability(char, sequence, sequences) {
		let p = {}
		this.sequences.forEach(function(seq) {
			let s = seq[char]
			p[s] = (p[s] > 0) ? p[s] += 1 : 1
		})
		return (p[sequence[char]] / this.sequences.length)
	}
	
	this.remove_all = function () {
		let logo = document.getElementById('logo');
		while (logo.firstChild) {
			logo.removeChild(logo.firstChild);
		}
	}

	this.create_logo = function (sequences) {
		// colors
		let hydrophobic = 'AVLIPWFM';
		let hColors = ['#000000', '#17202a','#1c2833', '#212f3d', '#273746',' #2c3e50', '#566573', '#808b96']
		
		let acidic = 'DE';
		let aColors = ['#c0392b','#e74c3c']
		
		let basic = 'KRH';
		let bColors = ['#2874a6', '#5dade2'];
		
		let neutral = 'QN';
		let nColors = ['#4a235a','#af7ac5']
		
		let polar = 'GSTYC';
		let pColors = ['#186a3b', '#239b56','#28b463','#58d68d','#82e0aa']
		let colors = ['#8AFBFF', '#8AFF9F', '#FF80C5', '#81AAFF', '#FF9E9E', '#C1BEFF', '#22FADD', '#4AFF9C', '#CF80C5', '#FF5E9E', '#41AAFF', '#91BEFF', '#EEBBFF', '#BAFF9C', '#2F80C5', '#3F9E9E', '#C1AAFF', '#41BEFF']

		
		//
		// Store unique chars from all sequences
		let all_chars = []
		this.sequences.forEach(function(seq) {
			for (var j = 0; j < seq.length; j++) {
				let s = seq[j]
				if (!all_chars.includes(s)) all_chars.push(s)
			}
		})
		
		let type = this.isClassic;
		
		//
		// Create div for each sequence index, append spans with
		// height relative to probability
		for (var i = 0; i < this.sequences[0].length; i++) {
			
			let div = document.createElement('div')

			if (type) {
				div.classList = 'logo classic'
			} else {
				div.className = 'logo'
			}
			//div.className = 'logo-classic'
			let p = {}
			let chars = []

			this.sequences.forEach(function(sequence) {
				let s = sequence.charAt(i)
				if (!chars.includes(s)) {
					p[s] = probability(i, sequence, sequences)
					chars.push(s)
				}
			})
         get_height = (prob) => height * prob
         get_color = (char) => colors[all_chars.indexOf(char)]
			get_scale_factor = () => height * 0.013
				function getAAColor(c) {

					if (hydrophobic.indexOf(c) > -1) { // Hydrophobic
						return hColors[hydrophobic.indexOf(c)];
					} else if (acidic.includes(c)) { // Acidic
						return aColors[acidic.indexOf(c)];
					} else if (basic.includes(c)) { // Basic
						return bColors[basic.indexOf(c)]
					} else if (neutral.includes(c)) {// Neutral
						return nColors[neutral.indexOf(c)]
					} else if (polar.includes(c)){ // Polar
						return pColors[polar.indexOf(c)]
					}
				}	
			
			if (type) {
            chars.reverse().forEach(function(char) {
               div.innerHTML += '<div style="height:' + get_height(p[char]) + 'px;"><span style=\"color:' + getAAColor(char) + ';transform: scaleX(1.1) scaleY(' + (get_scale_factor() * p[char]) + ');\">' + char + '</span></div>'
            })
			} else {
				chars.reverse().forEach(function(char) {
					c = String.fromCharCode(97 + char);
					div.innerHTML += '<span style="height:' + (height * p[char]) + 'px;background:' + getAAColor(char) + ';font-size:' + (1 + p[char]) + 'em;\">' + char + '</span>'
				})
			}
			

			document.getElementById('logo').appendChild(div)
		}
		//
		// After append, scroll to bottom
		document.getElementById('logo').scrollTop = document.getElementById('logo').scrollHeight
	}
	

	
	this.get_random_sequences = function () {
		let chars = ['A','A','A','F','L','K','K','K','P','P','G','T','L','S','V','R','M']
		let arr = []
		for (var i = 0; i < 9; i++) {
			let a = chars[Math.floor(Math.random() * chars.length)]
			for (var j = 0; j < 8; j++) {
				if (j == 3) a += 'P'
				else a += chars[Math.floor(Math.random() * chars.length)]
			}
			arr.push(a)
		}
		this.sequences = sequences;
		this.create_logo()
	}

	this.get_demo_sequences = function() {
		sequences = ['ALAKPAAAK','ALAKPAAAA', 'ALAKPAAAR', 'ALAKPAAAT', 'MLAKPAAAK', 'ALAKPAAMK', 'GILAPVFTK', 'TLNAPVKVV', 'KLNEPVLLL', 'AVVPPVVSV']
		this.sequences = sequences;
		this.create_logo()
	}
}